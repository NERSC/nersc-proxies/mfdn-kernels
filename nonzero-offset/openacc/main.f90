module m_serial
  implicit none
contains
  subroutine exclusive_scan(x,y,n,dt)
    use iso_fortran_env, only : dp => real64
    use m_timer
    integer, intent(in) :: n
    integer, dimension(n), intent(in) :: x
    integer, dimension(n), intent(out) :: y
    real(dp), intent(out) :: dt
    real(dp) :: t0
    integer :: i
    !$acc data present(x,y)
    t0 = wtime()
    !$acc serial
    y(1) = 0
    do i = 1, n-1
       y(i+1) = y(i) + x(i)
    end do
    !$acc end serial
    dt = wtime() - t0
    !$acc end data
  end subroutine exclusive_scan
end module m_serial

module m_scan
  implicit none
contains
  subroutine exclusive_scan(x,y,n,dt)
    use iso_fortran_env, only : dp => real64
    use m_timer
    integer, intent(in) :: n
    integer, dimension(n), intent(in) :: x
    integer, dimension(n), intent(out) :: y
    real(dp), intent(out) :: dt
    real(dp) :: t0
    integer :: j, tmp
    integer :: offset
    integer :: rshift
    !$acc data present(x,y)
    t0 = wtime()

    !$acc parallel loop async
    do j = 1, n
       y(j) = x(j)
    end do
    !$acc end parallel
    
    offset = 1
    ! sweep up, reduction in place
    do while (offset < n)
       !$acc parallel loop firstprivate(offset) async
       do concurrent (j=0:n-1:2*offset)
          y(j + 2*offset) = y(j + offset) + y(j + 2*offset)
       end do
       !$acc end parallel
       offset = 2*offset
    end do

    ! sweep down, complete the scan
    !$acc serial async
    y(n) = 0
    !$acc end serial
    offset = rshift(offset, 1)
    do while(offset > 0)
       !$acc parallel loop firstprivate(offset, tmp) async
       do concurrent(j=0:n-1:2*offset) local(tmp)
          tmp = y(j + offset)
          y(j + offset) = y(j + 2*offset)
          y(j + 2*offset) = tmp + y(j + 2*offset)
       end do
       !$acc end parallel
       offset = rshift(offset, 1)
    end do
    
    !$acc wait    
    dt = wtime() - t0
    !$acc end data
  end subroutine exclusive_scan
end module m_scan

program main
#if defined(SERIAL)
  use m_serial, only : exclusive_scan
#else
  use m_scan, only : exclusive_scan
#endif
  use iso_fortran_env, only : dp => real64
  implicit none  
  integer :: pnmax = 25
  integer :: niter = 100
  integer :: i,j,n
  integer(8) :: verify
  integer, allocatable :: a(:), b(:)
  real, allocatable :: x(:)
  
  real(dp) :: dt
  integer :: nargs
  character(len=32) :: arg

  nargs = command_argument_count()
  if (nargs >= 1) then
     if (nargs >= 1) then
        call get_command_argument(1, arg)
        read(arg, *) pnmax
     end if
     if (nargs >= 2) then
        call get_command_argument(2, arg)
        read(arg, *) niter
     end if
  end if

  write(*,'(*(g0, :, ","))') "iteration", "n", "time"

  n = 4
  do while(n < 2**pnmax)
     n = n * 2
     allocate(a(n), b(n), x(n))
     call random_number(x)
     a = floor(x*100.0)     
     b = 0
     do j = 1, n -1
        b(j+1) = b(j) + a(j)
     end do
     verify = sum(b)
     b = 0
     
     !$acc enter data copyin(a, b)
     deallocate(x)
     do i = 1, niter
        call exclusive_scan(a, b, n, dt)
        !$acc update host (b)
        if (verify .eq. sum(b)) then
           write(*, '(*(g0, :, ","))') i, n, dt
        else
           write(*,*) 'WRONG ANSWER', verify, sum(b)
           stop
        end if
     end do
     !$acc exit data delete(a, b)

     deallocate(a, b)
  end do
     
  
end program main
  
