ifndef COMPILER
define compiler_help
Set COMPILER to change flags. (defaulting to nvidia)
Available compilers are:
  nvidia cray
endef
$(info $(compiler_help))
COMPILER=nvidia
endif

ifndef TARGET
define target_help
Set TARGET to change device (defaulting to skylake)
Available TARGETS:
  skylake haswell v100 a100 mi100
Note that not all COMPILER, TARGET combinations are valid.
endef
$(info $(target_help))
TARGET=skylake
endif

COMPILER_nvidia = nvfortran
COMPILER_cray = ftn
FC = $(COMPILER_$(COMPILER))

FLAGS_nvidia_CPU = -Minfo -fast -Mpreprocess -acc=multicore -i8
FLAGS_nvidia_skylake = $(FLAGS_nvidia_CPU) -tp=skylake
FLAGS_nvidia_haswell = $(FLAGS_nvidia_CPU) -tp=haswell

FLAGS_nvidia_GPU = -Minfo -fast -Mpreprocess -acc=gpu -i8
FLAGS_nvidia_v100 = $(FLAGS_nvidia_GPU) -tp=skylake -gpu=cc70
FLAGS_nvidia_a100 = $(FLAGS_nvidia_GPU) -tp=zen -gpu=cc80

# see ./cray_env.sh
FLAGS_cray = -e F -h list=a -h acc -s integer64
FLAGS_cray_haswell = $(FLAGS_cray)
FLAGS_cray_skylake = $(FLAGS_cray)
FLAGS_cray_v100 = $(FLAGS_cray)
FLAGS_cray_mi100 = $(FLAGS_cray)


ifndef FLAGS_$(COMPILER)_$(TARGET)
$(error $(TARGET) with $(COMPILER) not supported)
endif

FCFLAGS = $(FLAGS_$(COMPILER)_$(TARGET))

benchmark: m_timer.f90 m_mbstate.f90 main.f90
	$(FC) $(FCFLAGS) -o benchmark.$(COMPILER).$(TARGET).x $^

.PHONY: clean
clean:
	rm -f *.o *.mod benchmark.*.*.x *.optrpt
