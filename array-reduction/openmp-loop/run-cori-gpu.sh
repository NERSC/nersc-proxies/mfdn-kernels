#!/bin/bash
#SBATCH --qos special
#SBATCH --exclusive
#SBATCH --constraint gpu
#SBATCH --gpus 1
#SBATCH --time-min 30
#SBATCH --time 240

for t in v100 skylake; do
    for v in manual atomic; do 	
	srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.$t.$v.x | tee nvidia.openmp-loop.$t.$v.csv
    done
done
