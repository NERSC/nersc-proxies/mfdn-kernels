#!/bin/bash
#SBATCH --job-name benchmark-omp
#SBATCH --time 30
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --gpus 1

for t in mi100; do
    for v in array manual atomic; do
	srun -n 1 --cpu-bind cores --gpu-bind=closest ./benchmark.cray.$t.$v.x | tee cray.openmp.$t.$v.csv
    done
done
