#!/bin/bash
#SBATCH --qos regular
#SBATCH --exclusive
#SBATCH --constraint dgx
#SBATCH --gpus 1
#SBATCH --time-min 10
#SBATCH --time 240

srun -n 1 --cpu-bind cores --sockets 1 -c 64 ./benchmark.nvidia.a100.x | tee nvidia.a100.parallel.csv
srun -n 1 --cpu-bind cores --sockets 1 -c 64 ./benchmark.nvidia.a100.serial.x | tee nvidia.a100.serial.csv
