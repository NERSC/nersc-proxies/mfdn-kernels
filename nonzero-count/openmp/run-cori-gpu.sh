#!/bin/bash
#SBATCH --qos special
#SBATCH --exclusive
#SBATCH --constraint gpu
#SBATCH --gpus 1
#SBATCH --time-min 60
#SBATCH --time 240

srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.v100.x | tee nvidia.openmp.v100.csv
srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.skylake.x | tee nvidia.openmp.skylake.csv
