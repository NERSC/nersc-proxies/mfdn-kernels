module m_timer
  implicit none
  private
  public wtime
contains
  function wtime()
    use iso_fortran_env, only : dp => real64, i8 => int64
    real(8) :: wtime
    integer(i8) :: count0, time_rate
    call system_clock(count0, time_rate)
    wtime = real(count0, kind=dp) / real(time_rate, kind=dp)
  end function wtime
end module m_timer
