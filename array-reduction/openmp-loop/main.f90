program main
#ifdef ATOMIC
  use m_array_reduction_atomic
#elif MANUAL
  use m_array_reduction_manual
#else
  use m_array_reduction
#endif
  use iso_fortran_env, only : dp => real64, sp => real32
  implicit none  
  integer :: pnmax = 20
  integer :: pmmax = 6
  integer :: niter = 5
  integer :: i,j,n,m

  real(sp), dimension(:), allocatable :: a
  real(sp), dimension(:,:), allocatable :: x, y

  real(dp), parameter :: max_time = 10.0_dp
  
  real(dp) :: dt, dtmin
  integer :: nargs
  character(len=32) :: arg

  logical :: flag

  nargs = command_argument_count()
  if (nargs >= 1) then
     if (nargs >= 1) then
        call get_command_argument(1, arg)
        read(arg, *) pnmax
     end if
     if (nargs >= 2) then
        call get_command_argument(2, arg)
        read(arg, *) niter
     end if
  end if

  write(*,'(*(g0, :, ","))') "iteration", "n", "m", "time"  

  flag = .false.
  n = 2
  do while(n < 2**pnmax)
     n = n * 2
     m = 1
     do while (m < 2**pmmax)
        m = m * 2
        allocate(a(m), x(m,n), y(m,n))
        call random_number(x)
        call random_number(y)
        !$omp target enter data map(to: x,y)
        do i = 1, niter
           call array_reduction(x, y, a, n, m, dt)
           write(*, '(*(g0, :, ","))') i, n, m, dt
           if (dt > max_time) flag = .true.
        end do
        !$omp target exit data map(delete: x,y)
        deallocate(a,x,y)
        if (flag) stop
     end do
  end do
     
  
end program main
  
