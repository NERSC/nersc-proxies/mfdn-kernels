#!/bin/bash
#SBATCH --qos regular
#SBATCH --exclusive
#SBATCH --constraint dgx
#SBATCH --gpus 1
#SBATCH --time-min 10
#SBATCH --time 240

for t in a100; do
    for v in manual atomic; do
	srun -n 1 --cpu-bind cores --sockets 1 -c 64 ./benchmark.nvidia.$t.$v.x | tee nvidia.openacc.$t.$v.csv
    done
done
