module m_mbstate
  implicit none
contains
  
  subroutine create_state(nparticles, mbstate, bitrep, bitrep_ext)
    use iso_fortran_env
    integer, intent(in) :: nparticles
    integer(int16), dimension(nparticles), intent(out) :: mbstate
    integer(int64), intent(out) :: bitrep, bitrep_ext

    integer, parameter :: bits =  64
    integer, parameter :: nsps = 128
    integer :: i, temp
    real    :: u, scale

    temp = 0
    scale = real(nsps) / nparticles
    do i = 1, nparticles
       call random_number(u)
       temp = temp + floor(1 + scale * u)
       mbstate(i) = temp
    end do
    
    bitrep = 0
    bitrep_ext = 0
    do i = 1, nparticles
       temp = mbstate(i)
       if (temp < bits) then
          bitrep = ibset(bitrep, temp)
       else
          bitrep_ext = ibset(bitrep_ext, temp-bits)
       end if
    end do

  end subroutine create_state
    
end module m_mbstate
