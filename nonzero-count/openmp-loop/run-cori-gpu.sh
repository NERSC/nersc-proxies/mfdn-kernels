#!/bin/bash
#SBATCH --qos special
#SBATCH --exclusive
#SBATCH --constraint gpu
#SBATCH --gpus 1
#SBATCH --time-min 40
#SBATCH --time 240

srun -t 10 -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.v100.use_bind.x | tee nvidia.openmp-loop-bind.v100.csv
srun -t 10 -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.v100.x | tee nvidia.openmp-loop.v100.csv
srun -t 10 -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.skylake.use_bind.x 16 5 | tee nvidia.openmp-loop-bind.skylake.csv
srun -t 10 -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.skylake.x 16 5 | tee nvidia.openmp-loop.skylake.csv
