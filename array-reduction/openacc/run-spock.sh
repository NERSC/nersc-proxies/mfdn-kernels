#!/bin/bash
#SBATCH --time 30
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --gpus 1

srun -n 1 --gpu-bind=closest ./benchmark.cray.mi100.atomic.x | tee cray.openacc.mi100.atomic.csv
