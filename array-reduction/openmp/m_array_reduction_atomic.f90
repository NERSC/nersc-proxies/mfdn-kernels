module m_array_reduction_atomic
  implicit none
contains
  subroutine array_reduction(x, y, a, n, m, dt)
    use iso_fortran_env, only : dp => real64, sp => real32
    use m_timer, only : wtime
    integer, intent(in) :: n, m
    reaL(sp), dimension(m, n), intent(in) :: x, y
    real(sp), intent(out) :: a(m)
    integer :: i,j,k
    real(dp), intent(out) :: dt
    real(dp) :: t0
    a(:) = 0.0
    !$omp target data map(to:x,y) map(tofrom:a)
    t0 = wtime()
    !$omp target teams distribute parallel do collapse(3)
    do i = 1, n
       do j = 1, n
          do k = 1, m
             !$omp atomic
             a(k) = a(k) + x(k,i) * y(k,j)
             !$omp end atomic
          end do
       end do
    end do
    !$omp end target teams distribute parallel do
    dt = wtime() - t0
    !$omp end target data

  end subroutine array_reduction
end module m_array_reduction_atomic
