#!/bin/bash
#SBATCH --qos special
#SBATCH --exclusive
#SBATCH --constraint gpu
#SBATCH --gpus 1
#SBATCH --time-min 30
#SBATCH --time 240

srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.v100.x | tee nvidia.v100.csv
srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.skylake.cpu.x | tee nvidia.skylake.no_atomic.csv
srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.skylake.x | tee nvidia.skylake.csv

